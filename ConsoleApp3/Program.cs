﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleApp3
{
    class Program
    {
        static int GetHealth(int first, int last, string d, string[] genes, int[] health)
        {
           

            int healthyPoint = 0;
            for(int i  = first; i <= last; i++) { 
                int count = CountMatches(d, genes[i]);
                
                healthyPoint += count * health[i];

            }
            return healthyPoint;

        }

        static void Main(string[] args)
        {
            int n = Convert.ToInt32(Console.ReadLine());

            string[] genes = Console.ReadLine().Split(' ');

            int[] health = Array.ConvertAll(Console.ReadLine().Split(' '), healthTemp => Convert.ToInt32(healthTemp))
            ;
            int s = Convert.ToInt32(Console.ReadLine());
            List<int> healthCollection = new List<int>();
            for (int sItr = 0; sItr < s; sItr++)
            {
                string[] firstLastd = Console.ReadLine().Split(' ');

                int first = Convert.ToInt32(firstLastd[0]);

                int last = Convert.ToInt32(firstLastd[1]);

                string d = firstLastd[2];
                healthCollection.Add(GetHealth(first, last, d, genes, health));
            }
            Console.Write($"{healthCollection.Min()} {healthCollection.Max()}");
        }

        public static int CountMatches(string searchInput, string text)
        {
            int count = 0;
            for(int i = 0; i < searchInput.Length - text.Length + 1; i++)
            {
                bool equals = true;
                
                for(int j = 0; j < text.Length; j++)
                {
                    if(searchInput[i + j] != text[j])
                    {
                        equals = false;
                        break;
                    } 
                }
                if(equals)
                {
                    count++;
                }
            }
            return count;
        }

    }
}
